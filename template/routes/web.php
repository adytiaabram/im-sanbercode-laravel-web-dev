<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('table');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});

Route::get('/table', function () {
    return view('table');
});

// Route::get('/master', function(){
//     return view('layout.master');
// });

//CRUD User

//Create Data Cast
//Route untuk mengarah ke form tambah user
Route::get('/cast/create', [CastController::class,'create']);
//Route untuk simpan data inputan ke table cast DB
Route::post('/cast', [CastController::class, 'store']);

//Read Cast
//Route untuk tampil semua data di table cast
Route::get('/cast', [CastController::class, 'index']);
//Route untuk Detail data berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update Cast
//Route untuk mengarah ke form edit user
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Route untuk update data berdasarkan di table cast DB
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Cast
//Route untuk delete cast berdasarkan id
Route::delete('/cast/{cast_id}', [CastController:: class, 'destroy']);

