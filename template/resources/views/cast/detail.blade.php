@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection


@section('content')
    <h1>{{$casts->nama}}</h1>
    <p>{{$casts->umur}}</p>
    <p>{{$casts->bio}}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection