@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
    <form action="/cast/{{$casts->id}}" method="POST">
    @method('PUT')
    @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" class="form-control" value="{{$casts->nama}}" name="nama">
        </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <label>Umur Cast</label>
            <input type="text" class="form-control" value="{{$casts->umur}}" name="umur">
        </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Bio Cast</label>
            <textarea name="bio" class="form-control">{{$casts->bio}}</textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection