<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('halaman.register');
    }
    
    public function masuk(Request $request)
    {
       $namaDepan = $request['fname'];
       $namaBelakang = $request['lname'];

       return view('halaman.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);

        // dd($request->all());
        // return view('halaman.welcome');
    }
}
